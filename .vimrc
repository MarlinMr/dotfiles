set number
set relativenumber
syntax on
set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
set list
set expandtab ts=4 sw=4 ai
set is hls
colorscheme koehler
execute pathogen#infect()
